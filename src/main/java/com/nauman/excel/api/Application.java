package com.nauman.excel.api;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.nauman.excel.api.db.ConnectionFactory;
import com.nauman.excel.api.db.JdbcTemplate;
import com.nauman.excel.api.db.MySqlDb;
import com.nauman.excel.api.db.jdbc.mapper.EmployeeMapper;
import com.nauman.excel.api.excel.writer.EmployeeExcelContentWriter;
import com.nauman.excel.api.excel.writer.ExcelWriter;
import com.nauman.excel.api.excel.writer.Writer;
import com.nauman.excel.api.exception.ApplicationException;
import com.nauman.excel.api.exception.JdbcException;
import com.nauman.excel.api.exception.WriteException;
import com.nauman.excel.api.info.Employee;
import com.nauman.excel.api.info.Student;

public class Application {

	private static final String fileName = "C:\\student.xlsx";

	public static void main(String[] args) throws ApplicationException, SQLException, InterruptedException {
		
		String option = args.length > 0 ? args[0] : "excelToDB";
		
		if ("excelToDB".equals(option)) {
			System.out.println("Excel to DB!");
			
			try {
				
				FileInputStream excelFile = new FileInputStream(new File(fileName));
				XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
				XSSFSheet sheet = workbook.getSheetAt(0);

				// Cell cellIndex = row.getCell(0);

				List<Student> students = new ArrayList<>();
				
				for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
					Row row = sheet.getRow(rowIndex); // Get Second Row.
					Student student = new Student();
					student.setId((int) row.getCell(0).getNumericCellValue());
					student.setFirstName(row.getCell(1).getStringCellValue());
					student.setLastName(row.getCell(2).getStringCellValue());
					student.setGender(row.getCell(3).getStringCellValue());
					student.setAge((int) row.getCell(4).getNumericCellValue());
					students.add(student);
				}
				
				workbook.close();
				
				final String insertQueryString = "INSERT INTO stu_tbl(id,firstName,lastName,gender,age)  VALUES(?,?,?,?,?)";
				final String updateQueryString = "UPDATE stu_tbl SET firstName = ?, lastName = ?, gender = ?, age = ? WHERE id = ?";
				Connection con = DriverManager.getConnection("jdbc:mysql://localhost/apachepoi", "dbuser", "dbpassword");
				System.out.println("---Connect to Database---");
				
				for (Student student : students) {
					PreparedStatement getStatement = con.prepareStatement("SELECT * FROM stu_tbl WHERE id = ?");
					getStatement.setInt(1, student.getId());
					ResultSet resultSet = getStatement.executeQuery();
					
					boolean newRecord = !resultSet.next();
					
					PreparedStatement statement = con.prepareStatement(newRecord ? insertQueryString : updateQueryString);
					int index = 1;

					if (newRecord) {
						statement.setInt(index++, student.getId());					
					}

					statement.setString(index++, student.getFirstName());
					statement.setString(index++, student.getLastName());
					statement.setString(index++, student.getGender());
					statement.setInt(index++, student.getAge());
					
					if (!newRecord) {
						statement.setInt(index++, student.getId());					
					}
					
					statement.executeUpdate();
					
					System.out.println("Synced students data");
				}

				

			} catch (Exception e) {
				e.printStackTrace();
			}
			
		} else if("DBToExcel".equals(option)) {
			System.out.println("DB To Excel!");
			DBToExcel();
		} else if("runScheduler".equals(option)) {
			System.out.println("Running Scheduler!");
			
			for (int i = 1; i > 0; i++) {
				Thread.sleep(2 * 1000); 
				System.out.println(i);
				DBToExcel();
			}
			
		}

		
		
		

	}
	
	private static void DBToExcel() throws ApplicationException {
		try {
			final String queryString = "SELECT * FROM emp_tbl";
			JdbcTemplate<Employee> jdbcTemplate = new JdbcTemplate<>(ConnectionFactory.getConnection(new MySqlDb()));
			List<Employee> employees = jdbcTemplate.query(queryString, new EmployeeMapper());

			Writer writer = new ExcelWriter();
			writer.write(new File("exceldatabase.xlsx"), new EmployeeExcelContentWriter(employees));
		} catch (SQLException | WriteException | JdbcException ex) {
			throw new ApplicationException(ex);
		}
	}

}
