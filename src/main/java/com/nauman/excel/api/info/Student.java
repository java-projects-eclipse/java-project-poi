package com.nauman.excel.api.info;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Student {
	private Integer id;
	private String firstName;
	private String lastName;
	private String gender;
	private Integer age;
	
}
