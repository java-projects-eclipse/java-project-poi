package com.nauman.excel.api.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.nauman.excel.api.db.jdbc.mapper.Mapper;
import com.nauman.excel.api.exception.JdbcException;

public class JdbcTemplate<T> {
	private Connection connection;

	public JdbcTemplate(final Connection connection) {
		this.connection = connection;
	}

	private Statement prepareStatement() throws SQLException {
		return getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
	}

	public List<T> query(final String queryString, final Mapper<T> mapper) throws JdbcException {
		List<T> response = new ArrayList<>();
		
		
		try (
				Statement statement = prepareStatement(); 
				ResultSet resultSet = statement.executeQuery(queryString);
				) {
			while (resultSet.next()) {
				response.add(mapper.map(resultSet));
			}
		} catch (SQLException ex) {
			throw new JdbcException(
					String.format("Unable to execute query: %s, Exception: %s", queryString, ex.getMessage()));
		}
		return response;
	}

	public Connection getConnection() {
		return connection;
	}
}
