package com.nauman.excel.api.db.jdbc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.nauman.excel.api.info.Employee;

public class EmployeeMapper implements Mapper<Employee> {

	@Override
	public Employee map(ResultSet resultSet) throws SQLException {
		Employee employee = new Employee();
		employee.setId(resultSet.getInt("EMP ID"));
		employee.setName(resultSet.getString("EMP NAME"));
		employee.setSalary(resultSet.getInt("Salary"));
		employee.setDesignation(resultSet.getString("DEG"));
		employee.setDepartment(resultSet.getString("DEPT"));
		return employee;
	}

}
