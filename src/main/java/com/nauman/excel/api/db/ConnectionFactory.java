package com.nauman.excel.api.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

	public static Connection getConnection(final Database database) throws SQLException {
		return DriverManager.getConnection(database.getConnectionString(), database.getUsername(),
				database.getPassword());
	}

}
