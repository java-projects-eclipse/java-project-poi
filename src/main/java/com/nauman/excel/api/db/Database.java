package com.nauman.excel.api.db;

public interface Database {
	String getConnectionString();
	String getUsername();
	String getPassword();
}
