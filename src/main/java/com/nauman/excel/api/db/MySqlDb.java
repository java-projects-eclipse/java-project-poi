package com.nauman.excel.api.db;

public class MySqlDb implements Database{

	@Override
	public String getConnectionString() {
		return "jdbc:mysql://localhost/apachepoi";
	}

	@Override
	public String getUsername() {
		return "dbuser";
	}

	@Override
	public String getPassword() {
		return "dbpassword";
	}

}
