package com.nauman.excel.api.exception;

public class ApplicationException extends Exception {

	public ApplicationException(String message) {
		super(message);
	}
	
	public ApplicationException(Exception exception) {
		super(exception.getMessage());
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
