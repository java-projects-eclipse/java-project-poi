package com.nauman.excel.api.exception;

public class WriteException extends Exception {

	public WriteException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
