package com.nauman.excel.api.exception;

public class JdbcException extends Exception {

	public JdbcException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
