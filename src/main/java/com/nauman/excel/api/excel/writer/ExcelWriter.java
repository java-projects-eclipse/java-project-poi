package com.nauman.excel.api.excel.writer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.nauman.excel.api.exception.WriteException;

public class ExcelWriter implements Writer {

	@Override
	public void write(final File file, final ContentWriter contentWriter) throws WriteException {
		try (
				XSSFWorkbook workbook = new XSSFWorkbook(); 
				FileOutputStream out = new FileOutputStream(file);
				) {
			contentWriter.write(workbook);
			workbook.write(out);
			System.out.println("exceldatabase.xlsx written successfully");
		} catch (IOException ex) {
			throw new WriteException(
					String.format("Unable to write excel file: %s, Exception: %s", file.getName(), ex.getMessage()));
		}
	}

}
