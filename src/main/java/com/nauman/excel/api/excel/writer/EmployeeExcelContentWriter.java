package com.nauman.excel.api.excel.writer;

import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.nauman.excel.api.info.Employee;

public class EmployeeExcelContentWriter implements ContentWriter {
	
	private List<Employee> employees;
	
	public EmployeeExcelContentWriter(final List<Employee> employees) {
		this.employees = employees;
	}

	@Override
	public void write(XSSFWorkbook workbook) {
		XSSFSheet spreadsheet = workbook.createSheet("Employe db");
		addHeader(spreadsheet);

		int rowIndex = 2;
		for (Employee employee : employees) {
			int cellIndex = 1;
			XSSFRow row = spreadsheet.createRow(rowIndex++); // Creating Second Row
			row.createCell(cellIndex++).setCellValue(employee.getId());
			row.createCell(cellIndex++).setCellValue(employee.getName());
			row.createCell(cellIndex++).setCellValue(employee.getDesignation());
			row.createCell(cellIndex++).setCellValue(employee.getSalary());
			row.createCell(cellIndex++).setCellValue(employee.getDepartment());
		}
	}
	
	private void addHeader(XSSFSheet spreadsheet) {
		XSSFRow row = spreadsheet.createRow(1);
		XSSFCell cell;
		cell = row.createCell(1);
		cell.setCellValue("EMP ID");
		cell = row.createCell(2);
		cell.setCellValue("EMP NAME");
		cell = row.createCell(3);
		cell.setCellValue("DEG");
		cell = row.createCell(4);
		cell.setCellValue("SALARY");
		cell = row.createCell(5);
		cell.setCellValue("DEPT");
	}

}
