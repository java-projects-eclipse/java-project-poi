package com.nauman.excel.api.excel.writer;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public interface ContentWriter {
	void write(XSSFWorkbook workbook);
}
