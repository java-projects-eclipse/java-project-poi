package com.nauman.excel.api.excel.writer;

import java.io.File;

import com.nauman.excel.api.exception.WriteException;

public interface Writer {
	void write(File file, ContentWriter contentWriter) throws WriteException;
}
